const LambdaTester = require('lambda-tester');
const updateHandler = require('../organizations/update').update;
const expect = require('chai').expect;
const AWSMock = require('aws-sdk-mock');
 
describe('Organization Update Handler', function() {
  const correctInput = '{\n\t"name": "organization3",\n\t"location": "location3"\n}\n';
  const invalidInput = '{\n\t"name": 123,\n\t"location": "location3"\n}\n';
  const incompleteInput = '{\n\t"location": "location3"\n}\n';

  it('updates organization', () => {
    AWSMock.mock('DynamoDB.DocumentClient', 'update', (params, callback) => {
      callback(null, {});
    });
    return LambdaTester(updateHandler)
      .event({ body: correctInput, pathParameters: { id: 1 } })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(200);
      });
  });

  it('throws error on updation if validation error', () => {
    return LambdaTester(updateHandler)
      .event({ body: invalidInput, pathParameters: { id: 1 } })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  it('throws error on updation if required params are missing', () => {
    return LambdaTester(updateHandler)
      .event({ body: incompleteInput, pathParameters: { id: 1 } })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  it('throws error on updation if required path params are missing', () => {
    return LambdaTester(updateHandler)
      .event({ body: correctInput })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  after(() => {
    AWSMock.restore();
  });
});