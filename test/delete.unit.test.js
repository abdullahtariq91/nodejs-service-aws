const LambdaTester = require('lambda-tester');
const deleteHandler = require('../organizations/delete').delete;
const expect = require('chai').expect;
const AWSMock = require('aws-sdk-mock');
 
describe('Organization Delete Handler', function() {
  it('deletes organization', () => {
    AWSMock.mock('DynamoDB.DocumentClient', 'delete', (params, callback) => {
      callback(null, {});
    });
    return LambdaTester(deleteHandler)
      .event({ pathParameters: { id: 1 } })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(200);
      });
  });

  it('throws error on deletion if path params are missing', () => {
    return LambdaTester(deleteHandler)
      .event({ })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  after(() => {
    AWSMock.restore();
  });
});