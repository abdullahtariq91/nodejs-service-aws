const LambdaTester = require('lambda-tester');
const createHandler = require('../organizations/create').create;
const expect = require('chai').expect;
const AWSMock = require('aws-sdk-mock');
 
describe( 'Organization Create Handler', function() {
  const correctInput = '{\n\t"name": "organization3",\n\t"location": "location3"\n}\n';
  const invalidInput = '{\n\t"name": 123,\n\t"location": "location3"\n}\n';
  const incompleteInput = '{\n\t"location": "location3"\n}\n';

  it('creates a new organization', () => {
    AWSMock.mock('IAM', 'createPolicy', (params, callback) => {
      callback(null, { Policy: { PolicyId: 'test'  } });
    });
    AWSMock.mock('DynamoDB.DocumentClient', 'put', (params, callback) => {
      callback(null, {});
    });
    return LambdaTester(createHandler)
      .event({ body: correctInput })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(200);
      });
  });

  it('throws error on creation if validation error', () => {
    return LambdaTester(createHandler)
      .event({ body: invalidInput })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  it('throws error on creation if required params are missing', () => {
    return LambdaTester(createHandler)
      .event({ body: incompleteInput })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  after(() => {
    AWSMock.restore();
  });
});