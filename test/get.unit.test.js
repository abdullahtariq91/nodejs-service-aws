const LambdaTester = require('lambda-tester');
const getHandler = require('../organizations/get').get;
const expect = require('chai').expect;
const AWSMock = require('aws-sdk-mock');
 
describe('Organization Get Handler', function() {
  it('retrieves organization', () => {
    AWSMock.mock('DynamoDB.DocumentClient', 'get', (params, callback) => {
      callback(null, { Item: { name: 'organization' } });
    });
    return LambdaTester(getHandler)
      .event({ pathParameters: { id: 1 } })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(200);
      });
  });

  it('throws error on retrieval if path params are missing', () => {
    return LambdaTester(getHandler)
      .event({ })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(400);
      });
  });

  after(() => {
    AWSMock.restore();
  });
});