const LambdaTester = require('lambda-tester');
const listHandler = require('../organizations/list').list;
const expect = require('chai').expect;
const AWSMock = require('aws-sdk-mock');
 
describe('Organization List Handler', function() {
  it('retrieves organizations', () => {
    AWSMock.mock('DynamoDB.DocumentClient', 'scan', (params, callback) => {
      callback(null, {});
    });
    return LambdaTester(listHandler)
      .event({ })
      .expectResult((result) => {
        expect(result.statusCode).to.eq(200);
      });
  });
  
  after(() => {
    AWSMock.restore();
  });
});