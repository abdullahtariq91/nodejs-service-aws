FROM circleci/node:8
USER root

WORKDIR /usr/src/app
COPY package*.json ./
COPY . .
ENV IS_OFFLINE=true

RUN apt-get update
RUN apt-get install -y default-jre
RUN apt-get install -y python
RUN apt-get install python-pip
RUN apt-get install python-dev
RUN pip install awscli --upgrade

RUN npm install
RUN npm install -g serverless mocha
RUN sls dynamodb install

EXPOSE 8080