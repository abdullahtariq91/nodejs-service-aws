'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');

module.exports.create = (event, context, callback) => {
  let options = {};
  
  // check if deployment is local or not
  if (process.env.IS_OFFLINE) {
    options.region = 'localhost';
    options.endpoint = 'http://localhost:8000'
  } else {
    options.region = 'us-east-1';
  }
  
  const dynamoDb = new AWS.DynamoDB.DocumentClient(options);
  const iam = new AWS.IAM({ apiVersion: '2010-05-08' });

  const timestamp = new Date().getTime();
  let organization = JSON.parse(event.body);
  organization.id = uuid.v1();
  const policyName = 'organizationPolicy-' + organization.id;

  // validate input
  if (typeof organization.name !== 'string' || typeof organization.location !== 'string') {
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        success: false,
        message: 'Couldn\'t create organization - Invalid Parameters.'
      })
    });
    return;
  }

  const organizationPolicy = {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": [
          "dynamodb:DeleteItem",
          "dynamodb:GetItem",
          "dynamodb:PutItem",
          "dynamodb:Scan",
          "dynamodb:UpdateItem"
        ],
        "Resource": "arn:aws:dynamodb:*:*",
        "Condition": {
          "ForAllValues:StringEquals": {
            "dynamodb:LeadingKeys": [
              "${organization.id}"
            ]
          }
        }
      }
    ]
  };
  
  const policyParams = {
    PolicyDocument: JSON.stringify(organizationPolicy),
    PolicyName: policyName,
  };
  
  iam.createPolicy(policyParams, (error, data) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          success: false,
          message: 'Couldn\'t create organization.'
        })
      });
      return;
    } else {
      const objectParams = {
        TableName: process.env.DYNAMODB_TABLE,
        Item: {
          organizationId: organization.id,
          name: organization.name,
          location: organization.location,
          policyId: data.Policy.PolicyId,
          createdAt: timestamp,
          updatedAt: timestamp
        },
      };

      dynamoDb.put(objectParams, (error) => {
        // handle potential errors
        if (error) {
          console.error(error);
          callback(null, {
            statusCode: error.statusCode || 501,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
              success: false,
              message: 'Couldn\'t create organization.'
            })
          });
          return;
        }
    
        // create a response
        const response = {
          statusCode: 200,
          body: JSON.stringify({
            success: true,
            organization: objectParams.Item
          }),
        };
        callback(null, response);
      });
    }
  });
};
