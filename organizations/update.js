'use strict';

const AWS = require('aws-sdk');

module.exports.update = (event, context, callback) => {
  let options = {};
  
  if (process.env.IS_OFFLINE) {
    options.region = 'localhost';
    options.endpoint = 'http://localhost:8000'
  } else {
    options.region = 'us-east-1';
  }
  
  const dynamoDb = new AWS.DynamoDB.DocumentClient(options);
  
  const timestamp = new Date().getTime();
  const data = JSON.parse(event.body);

  if (event.pathParameters === undefined) {
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        success: false,
        message: 'Couldn\'t update organization.'
      })
    });
    return;
  }

  if (typeof data.name !== 'string' || typeof data.location !== 'string') {
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        success: false,
        message: 'Couldn\'t update organization - Invalid Parameters.'
      })
    });
    return;
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      organizationId: event.pathParameters.id,
    },
    ConditionExpression: 'organizationId = :organizationId',
    ExpressionAttributeNames: {
      '#name_text': 'name',
      '#location_text': 'location'
    },
    ExpressionAttributeValues: {
      ':name': data.name,
      ':location': data.location,
      ':updatedAt': timestamp,
      ':organizationId': event.pathParameters.id
    },
    UpdateExpression: 'SET #name_text = :name, #location_text = :location, updatedAt = :updatedAt',
    ReturnValues: 'ALL_NEW',
  };

  // update the organization in the database
  dynamoDb.update(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          success: false,
          message: 'Couldn\'t update organization.'
        })
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify({
        success: true,
        organization: result.Attributes
      }),
    };
    callback(null, response);
  });
};
