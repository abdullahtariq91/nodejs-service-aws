'use strict';

const AWS = require('aws-sdk');

module.exports.list = (event, context, callback) => {
  let options = {};
  
  if (process.env.IS_OFFLINE) {
    options.region = 'localhost';
    options.endpoint = 'http://localhost:8000'
  } else {
    options.region = 'us-east-1';
  }
  
  const dynamoDb = new AWS.DynamoDB.DocumentClient(options);

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
  };
  // retrieve all organizations from the database
  dynamoDb.scan(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          success: false,
          message: 'Couldn\'t retrieve organizations.'
        })
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify({
        success: true,
        organizations: result.Items
      }),
    };
    callback(null, response);
  });
};
