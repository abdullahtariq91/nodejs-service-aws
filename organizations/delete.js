'use strict';

const AWS = require('aws-sdk');

module.exports.delete = (event, context, callback) => {
  let options = {};
  
  if (process.env.IS_OFFLINE) {
    options.region = 'localhost';
    options.endpoint = 'http://localhost:8000'
  } else {
    options.region = 'us-east-1';
  }
  
  const dynamoDb = new AWS.DynamoDB.DocumentClient(options);

  if (event.pathParameters === undefined) {
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        success: false,
        message: 'Couldn\'t remove organization.'
      })
    });
    return;
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      organizationId: event.pathParameters.id,
    },
  };

  // delete organization from the database
  dynamoDb.delete(params, (error) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          success: false,
          message: 'Couldn\'t remove organization.'
        })
      });
      return;
    }

    // create a response
    const response = {
      statusCode: 200,
      body: JSON.stringify({
        success: true,
        message: 'Organization deleted.'
      }),
    };
    callback(null, response);
  });
};
