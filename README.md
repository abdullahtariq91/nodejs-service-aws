# Node.js API using AWS Lambda & API Gateway

## Implementation
Created an API using AWS Lambda and API Gateway to manage `organization` entities. The `organizationId` is generated using `uuid` node module, and an IAM policy for multi-tenancy in DynamoDB. User can perform CRUD operations.

The code is tested using `lambda-tester` and `chai` modules, and by mocking AWS functionalities of DynamoDB and IAM Policy Creation.

## Requirements
1. Node.js >= 8.11.2
2. AWS CLI
3. Serverless Offline
4. Serverless DynamoDB Offline
5. AWS SDK
6. AWS SDK Mock
7. Mocha

## Folder Structure
```
|-- offline
|   |-- migrations
|       |-- organizations.json // used for DynamoDB migrations
|-- organizations
|   |-- create.js // create handler
|   |-- delete.js // delete handler
|   |-- get.js // get handler
|   |-- list.js // list handler
|   |-- update.js // update handler
|-- test
|   |-- create.unit.test.js
|   |-- delete.unit.test.js
|   |-- get.unit.test.js
|   |-- list.unit.test.js
|   |-- update.unit.test.js
|-- serverless.yml // yml file to generate Lambda functions
|-- package.json
|-- README.md
```

## Setup
### Docker Installation
#### 1. Unzip or Clone Project
Unzip or clone project, and open it in terminal.

#### 2. Build Docker Image
Run the following command `docker build -t <enter name> .`

#### 3. Run Docker
Run the following command to run the Docker image `docker run -p 49160:8080 -d <enter name> tail -f /dev/null`.

#### 4. Get Container ID
Run `docker -ps a` to get the ID of the container.

#### 5. Access the Container
Access container with the following command `docker exec -it <id> /bin/bash`.

#### 6. Create User
Create new user using AWS Console, and grant the user AdministratorAccess (no preset policy to create CloudFormation).

#### 7. Configure AWS CLI
Run `aws configure` on the terminal, and enter keys.

#### 8. Deploy to AWS
Run `sls deploy` to deploy project on AWS.
If executed successfully, endpoints will be generated on the terminal.

#### 9. Deploy Locally
Run `sls offline start` to deploy locally.
If executed successfully, endpoints will be generated on the terminal.

#### 10. Run Tests
Run tests by using the `mocha test` command.

### Manual Installation
#### 1. Unzip or Clone Project
Unzip or clone project, and open it in terminal.

#### 2. Install Requirements
Install npm modules by running `npm install` and then `serverless dynamodb install` on the terminal.

#### 3. Create User
Create new user using AWS Console, and grant the user AdministratorAccess (no preset policy to create CloudFormation).

#### 4. Configure AWS CLI
Make sure you have [aws-cli](https://aws.amazon.com/cli/) installed. Run `aws configure` on the terminal, and enter keys.

#### 5. Set ENV Variable
Add `export IS_OFFLINE=true` at the end of your `~/.bashrc` file.

#### 6. Deploy to AWS
Run `sls deploy` to deploy project on AWS.
If executed successfully, endpoints will be generated on the terminal.

#### 7. Deploy Locally
Run `sls offline start` to deploy locally.
If executed successfully, endpoints will be generated on the terminal.

#### 8. Run Tests
Run tests by using the `mocha test` command.

## Assumptions

### Multi-Tenancy
> If an organization is created an AWS IAM policy should be created to restrict the access to the
organization’s data in the NoSQL database.

The AWS IAM policy created is being used for multi-tenancy in DynamoDB. This project only creates an IAM policy with `LeadingKeys` condition which can only be met with `organizationId` - it does not provide a usage (manage other entities related to `organization`) of this IAM policy.

### NoSQL - DynamoDB
> The service should run on AWS

DynamoDB was chosen as the entire service was to be run on AWS.